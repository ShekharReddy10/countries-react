import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import "bootstrap/dist/css/bootstrap.min.css"
import "./Styles.css"

function NavBar(props) {
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid className='container'>
        <div>
          <Nav>
            <Navbar.Brand href="#">SOMEWHERE IN THE WORLD..?</Navbar.Brand>
          </Nav>
        </div>
        <div>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Form className="d-flex">
              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: '100px' }}
                navbarScroll
              >
                <Form.Control
                  type="search"
                  placeholder="Search Country"
                  className="me-2"
                  aria-label="Search"
                />
                <NavDropdown title={props.region} id="navbarScrollingDropdown">
                <NavDropdown.Item  onClick={()=>{props.onRegion("All Regions")}}>All regions</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item  onClick={()=>{props.onRegion("Americas")}}>America</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={()=>{props.onRegion("Asia")}}>Asia</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={()=>{props.onRegion("Europe")}}>Europe</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={()=>{props.onRegion("Africa")}}>Africa</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={()=>{props.onRegion("Oceania")}}>Oceania</NavDropdown.Item>
                </NavDropdown>
              </Nav>

            </Form>
          </Navbar.Collapse>
        </div>
      </Container>
    </Navbar>
  );
}

export default NavBar;