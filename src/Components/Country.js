import React, { Component } from 'react'
import Popup from './Popup';
import "./Styles.css"

class Country extends Component {
    render() {
        const { country } = this.props
       
        return (
            <div className="card" style={{ height: "350px" }}>
                <img src={country.flags.png} className="card-img-top country-flag" alt={country.name.official} onClick={() => this.props.showPopup(country,true)}/>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item country-name">{country.name.official}</li>
                    <li className="list-group-item">Population: {country.population}</li>
                    <li className="list-group-item">Region: {country.region}</li>
                    <li className="list-group-item">Capital: {country.capital}</li>
                </ul>
            </div>
        )
    }
}

export default Country