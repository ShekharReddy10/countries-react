import React, { Component } from 'react'
import "./Styles.css"

export class Popup extends Component {
  render() {
    document.addEventListener('keydown', this.props.onClosePopup)
    const { country } = this.props
    console.log(this.props)
    return (
      <div>
        <div className='overlay' onClick={() => { this.props.showPopup(country, false) }}></div>
        <div className="modal-show">
          <div >
            <div className='popup-header'>
              <h4>{country.country.name.official}</h4>
              <button className='close-popup' onClick={() => { this.props.showPopup(country, false) }}>Close</button>
            </div>
            <div className='popup-body'>
              <img style={{ maxHeight: "200px", width: "400px" }} src={country.country.flags.png} alt={country.country.name.official} />
              <ul className="list-group list-group-flush">
                {/* <li className="list-group-item country-name">{country.name.official}</li> */}
                <li className="list-group-item">Population: {country.country.population}</li>
                <li className="list-group-item">Region: {country.country.region}</li>
                <li className="list-group-item">Capital: {country.country.capital}</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Popup

