import React, { Component } from 'react'
import axios from "axios"
import Country from './Country'
import "./Styles.css"
import Loader from './Loader'
import Popup from './Popup'
import NavBar from './NavBar'
class Countries extends Component {
    constructor(props) {
        super(props)

        this.state = {
            countries: [],
            errorMsg: '',
            popUp: [],
            region: "All Regions"
        }
    }
    componentDidMount() {
        axios.get('https://restcountries.com/v3.1/all')
            .then((response) => {
                console.log(response)
                this.setState({ countries: response.data })
            })
            .catch((error) => {
                console.error(error)
                this.setState({ errorMsg: "Error while retrieving data" })
            })
    }
    popupHandler = (country, flag) => {
        flag ?
            this.setState({ popUp: [...this.state.popUp, { country }] }) :
            this.setState({ popUp: [] })
    }
    closePopupHandler =(Event)=>{
        console.log(Event)
        if(Event.key =="Escape"){
            this.setState({popUp:[]})
        }
    }
    regionHandler = (regionNavBar => {
        this.setState({ region: regionNavBar })
    })
    
    render() {
        const { countries, errorMsg, popUp, region } = this.state
        console.log(popUp)
        return (
            <div>
                <NavBar onRegion={this.regionHandler} region={region} />
                <div className='country-container'>
                    {
                        countries.length ?
                            countries.map(country => {
                                if (region == "All Regions") {
                                    return <Country key={country.name.official} country={country} showPopup={this.popupHandler} />
                                } else {
                                    if (region == country.region) {
                                        return <Country key={country.name.official} country={country} showPopup={this.popupHandler} />
                                    }
                                }
                            }) : <div className='loader'> <Loader /> </div>
                    }
                    {
                        errorMsg ? <div>{errorMsg}</div> : null
                    }
                    {
                        popUp.length ? <div><Popup country={popUp[0]} showPopup={this.popupHandler} onClosePopup={this.closePopupHandler}/> </div> : null
                    }
                </div>
            </div>
        )
    }
}

export default Countries